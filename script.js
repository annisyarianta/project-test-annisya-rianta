$(document).ready(function() {
    // Atur parameter untuk API
    var apiUrl = 'https://suitmedia-backend.suitdev.com/api/ideas';
    var params = {
        'page[number]': 1,
        'page[size]': 10,
        'append[]': ['small_image', 'medium_image'],
        'sort': '-published_at' // or 'published_at' for ascending order
    };

    // permintaan ke API dengan AJAX
    $.ajax({
        url: apiUrl,
        type: 'GET',
        data: params,
        success: function(response) {
            // Proses data response
            console.log(response);
        },
        error: function(error) {
            // Tangani kesalahan
            console.error('Error:', error);
        }
    });
});